class AddTicketsToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :tickets, :string
  end
end
