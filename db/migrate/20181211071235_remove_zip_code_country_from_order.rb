class RemoveZipCodeCountryFromOrder < ActiveRecord::Migration[5.2]
  def change
  	remove_column :orders, :zip_code, :string
    remove_column :orders, :country, :string
  end
end
