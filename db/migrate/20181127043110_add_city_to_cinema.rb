class AddCityToCinema < ActiveRecord::Migration[5.2]
  def change
    add_reference :cinemas, :city, foreign_key: true
  end
end
