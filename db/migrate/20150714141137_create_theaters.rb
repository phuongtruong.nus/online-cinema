class CreateTheaters < ActiveRecord::Migration[5.2]
  def change
    create_table :theaters do |t|
      t.string :number
      t.integer :capacity

      t.timestamps null: false
    end
  end
end
