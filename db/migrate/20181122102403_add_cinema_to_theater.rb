class AddCinemaToTheater < ActiveRecord::Migration[5.2]
  def change
    add_reference :theaters, :cinema, foreign_key: true
  end
end
