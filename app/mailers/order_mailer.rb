class OrderMailer < ApplicationMailer
  default from: 'duonghuynhtdt@gmail.com'

  def send_receipt(order, email)
    @order = order
    @email = email
    mail(to: @email, subject: 'Your receipt from Lotte Cinema')
  end
end


