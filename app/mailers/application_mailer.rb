class ApplicationMailer < ActionMailer::Base
  add_template_helper(OrdersHelper)
  add_template_helper(ShowtimesHelper)

  default from: "duonghuynhtdt@gmail.com"
  layout 'mailer'
end
