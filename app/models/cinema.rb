class Cinema < ActiveRecord::Base
  has_many :theaters
  belongs_to :city
end
