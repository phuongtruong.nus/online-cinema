class Theater < ActiveRecord::Base
  has_many :showtimes, dependent: :destroy
  belongs_to :cinema
end
